package io.classpath.servletdemo;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DbServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String	cookiename	=	null;
		Cookie[]	cookies	=	request.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getName().equals("name")) 
				cookiename	=	cookie.getValue();
		}
		
		PrintWriter	writer	=	response.getWriter();
		writer.write("The user name from cookie is "+cookiename);
		
	}
}
