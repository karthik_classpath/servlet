package io.classpath.servletdemo;

import java.io.IOException;



import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class XmlServerPath extends	HttpServlet{
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter	writer	=	response.getWriter();
		
		Cookie	cookie	=	new	Cookie("name", "Alex");
		response.addCookie(cookie);
		
		ServletConfig	config=	getServletConfig();
		//writer.write("Hi "+config.getInitParameter("Name"));
		/*
		 * String nameString = request.getParameter("userName");
		 * 
		 * HttpSession session = request.getSession(); ServletContext context =
		 * request.getServletContext();
		 * 
		 * if(nameString != "" && nameString != null) {
		 * session.setAttribute("savedName", nameString);
		 * context.setAttribute("savedName", nameString); }
		 * 
		 * writer.write("Welcome "+nameString);
		 * writer.write("<br> Session parameter has user name as "+(String)session.
		 * getAttribute("savedName"));
		 * writer.write("<br> Context parameter has user name as "+(String)context.
		 * getAttribute("savedName"));
		 */
//		RequestDispatcher	requestDispatcher	=	request.getRequestDispatcher("db");
//		requestDispatcher.forward(request, response);
		
		response.sendRedirect("db");
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//response.getWriter().append("Served at: ").append(request.getContextPath());
		response.setContentType("text/html");
		PrintWriter	writer	=	response.getWriter();
		
		String	nameString	=	request.getParameter("userName");
		writer.write("Welcome "+nameString+" from POST method <br>");
		
		String	deptString	=	request.getParameter("dept");
		writer.write("Working in the "+deptString+" department");
		
		String	profString	=	request.getParameter("prof");
		writer.write("<br> You are a  "+profString);
		
		
		String[]	locStrings	=	request.getParameterValues("loc");
		writer.write("<br>You are at "+locStrings.length+" places<br>");
		for (String string : locStrings) {
			writer.write(string+" ");
		}
		
	}
}
