package io.classpath.servletdemo;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

/**
 * Servlet implementation class Anything
 */
@WebServlet("/Anything")
public class Anything extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Anything() {
        // TODO Auto-generated constructor stub
    }

}
