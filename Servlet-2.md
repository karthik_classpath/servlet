#   Differences between GET and POST
Get method	|Post method
-----------|------------
Get Request sends the request parameter as query string appended at the end of the request.	|Post request send the request parameters as part of the http request body.
Get method is visible to every one.|	Post method variables are not displayed in the URL.
Restriction on form data, only ASCII characters allowed.	|No Restriction on form data, Binary data is also allowed.
Get methods have maximum size is 2000 character.|	Post methods have maximum size is 8 mb.
Restriction on form length, So URL length is restricted	|No restriction on form data.
Remain in browser history.|	Never remain the browser history.
Generally is used to query or to get some information from the server|Generally used to update or post some information to the server
Should be able to be repeated safely many times|Operations requested through POST can have side effects for which the user can be held accountable.

#   Tomcat's objects
-   The servlet runs inside the Tomcat container
-   Tomcat takes care of initialisation of the servlet also creates the **request** and **response** classes which are 
implementations of HttpServletRequest and HttpServletResponse respectively.
![](Images\19.png)
-   If another browser calls the same URL, then Tomcat creates a new request and response.
-   If it calls the URL another time, Tomcat doesn't reuse the objects but creates a new one - i.e. They are **created per access**
-   The servlet isn't created multiple times they are **reused**.

#   HttpSession interface
-   Container creates a session id for each user.The container uses this id to identify the particular user.
-   An object of HttpSession can be used to perform two tasks:
    -   Bind objects
    -   View and manipulate information about a session, such as the session identifier, creation time, and last accessed time.
    
![](https://static.javatpoint.com/images/httpsession.JPG)
-   ```public HttpSession getSession()```: Returns the current session associated with this request, or if the request does not have a session, creates one.
-   ```public HttpSession getSession(boolean create)```: Returns the current HttpSession associated with this request or, if there is no current session and create is true, returns a new session.


#   ServletContext Interface
-   An object of ServletContext is created by the web container at time of deploying the project.
-   This object can be used to get configuration information from web.xml file.
-   There is only one ServletContext object per web application.
-   Easy to maintain if any information is shared to all the servlet, it is better to make it available for all the servlet.

![](https://static.javatpoint.com/images/servletcontext.JPG)

-----------
<br>

#   RequestDispatcher
-   RequestDispatcher interface provides the facility of dispatching the request to another resource it may be html, servlet or jsp.
-   This interface can also be used to include the content of another resource also.
-   It is one of the way of servlet collaboration.
-   **forward()**
        
    Forwards a request from a servlet to another resource (servlet, JSP file, or HTML file) on the server.
![](https://static.javatpoint.com/images/forward.JPG)
    Response of second servlet is sent to the client. Response of the first servlet is not displayed to the user.
-   **include()**
    
    Includes the content of a resource (servlet, JSP page, or HTML file) in the response.
    ![](https://static.javatpoint.com/images/include.JPG)
    Response of second servlet is included in the response of the first servlet that is being sent to the client.
#   sendRedirect
-   The sendRedirect() method of HttpServletResponse interface can be used to redirect response to another resource, it may be servlet, jsp or html file.
-   It accepts relative as well as absolute URL.
-   It works at client side because it uses the url bar of the browser to make another request.

![](https://qph.fs.quoracdn.net/main-qimg-70265eb1cb405ee6bc1180e853a7cf61)

------------
<br>

#   ServletConfig Interface
-   An object of ServletConfig is created by the web container for each servlet. This object can be used to get configuration information from web.xml file.
-   If the configuration information is modified from the web.xml file, we don't need to change the servlet. 
-----------
<br>

#   Cookies
-   A cookie is a small piece of information that is persisted between the multiple client requests.
-   A cookie has a name, a single value, and optional attributes such as a comment, path and domain qualifiers, a maximum age, and a version number.

![](https://3.bp.blogspot.com/-EjCYPdeQSw8/W8MbngK50yI/AAAAAAAACJA/nfvik654o242A3gMlI3dnZfS4NkkEv_VACLcBGAs/s1600/Screenshot%2B%252878%2529.png)

------------------------
<br>

#   Servlet Filter   
-   A filter is an object that is invoked at the preprocessing and postprocessing of a request.
-   It is mainly used to perform filtering tasks such as conversion, logging, compression, encryption and decryption, input validation etc.
-   The servlet filter is **pluggable**.

![](https://static.javatpoint.com/images/filter.JPG)
Method	|Description
--------|-------
public void init(FilterConfig config)	|This is invoked only once. It is used to initialize the filter.
public void doFilter(HttpServletRequest request,HttpServletResponse response, FilterChain chain)	|This is invoked every time when user request to any resource, to which the filter is mapped. It is used to perform filtering tasks.
public void destroy()	|This is invoked only once when filter is taken out of the service.