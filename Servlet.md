#   Servlet
-   Servlets provide a component-based, platform-independent method for building Webbased applications.
-   Servlets have access to the entire family of Java APIs, including the JDBC API to access enterprise databases..
-   Using Servlets, you can collect input from users through web page forms, present records from a database or another source, and create web pages dynamically.
#   Apache Tomcat
-   Apache Tomcat is a free and open-source implementation of the Java Servlet, JavaServer Pages, Java Expression Language and WebSocket technologies.
-    Tomcat provides a "pure Java" HTTP web server environment in which Java code can run.
#   HttpServlet
-   The HttpServlet class extends the GenericServlet class and implements Serializable interface.
-   Any application server that we use such as Tomcat, Jboss, etc, it has have an implementation of HttpServlet 
-   It provides http specific methods such as doGet, doPost, doHead, doTrace etc.\
-   protected void **doGet**(HttpServletRequest req, HttpServletResponse res) <br>Handles the GET request. It is invoked by the web container.
-   protected void **doPost**(HttpServletRequest req, HttpServletResponse res) <br>Handles the POST request. It is invoked by the web container.
##   Interface HttpServletRequest
-   Extends the ServletRequest interface to provide request information for HTTP servlets.
-   The servlet container creates an HttpServletRequest object and passes it as an argument to the servlet's service methods (doGet, doPost, etc).
##  Interface HttpServletResponse
-   Extends the ServletResponse interface to provide HTTP-specific functionality in sending a response.
##  PrintWriter class
Java PrintWriter class is the implementation of Writer class. It is used to print the formatted representation of objects to the text-output stream.

-----
<br>

#   @WebServlet Annotation   
The @WebServletannotation is used to declare a servlet. The annotated class **must extend** the javax.servlet.http.HttpServlet class.
```java
@WebServlet("/ServletDemo")
```
```java
@WebServlet(
        name = "MyOwnServlet",
        description = "This is my first annotated servlet",
        urlPatterns = "/processServlet"
)
```
-------
<br>

# Deployment Descriptor (web.xml file)
   The deployment descriptor is an xml file, from which Web Container gets the information about the servet to be invoked.
   
![](https://i2.wp.com/way2java.com/wp-content/uploads/2014/02/image6.png)

-------------------
<br>

#       Servlet Life Cycle
![](https://static.javatpoint.com/images/servletlife.jpg)
- There are three states of a servlet: **new**, **ready** and **end**.
   - The servlet is in new state if servlet instance is created.
   -    After invoking the init() method, Servlet comes in the ready state. In the ready state, servlet performs all the tasks.
   -    When the web container invokes the destroy() method, it shifts to the end state.

##      1. Servlet class is loaded
-   The classloader is responsible to load the servlet class. 
-   The servlet class is loaded when the first request for the servlet is received by the web container.
##      2. Servlet instance is created
-    The web container creates the instance of a servlet after loading the servlet class. 
-    The servlet instance is **created only once** in the servlet life cycle.
##      3. init method is invoked
-  The web container calls the init method only once after creating the servlet instance. 
- The init method is used to **initialize** the servlet. 
- It is the life cycle method of the javax.servlet.Servlet interface. 
```java
public void init(ServletConfig config) throws ServletException 
``` 
##      4. service method is invoked
-  The web container calls the service method each time when request for the servlet is received.
- If servlet is not initialized, it follows the first three steps as described above then calls the service method. If servlet is initialized, it calls the service method.
```java 
public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException 
```
![](Images/70.png)
##      5. destroy method is invoked
- The web container calls the destroy method before removing the servlet instance from the service.
- It gives the servlet an opportunity to clean up any resource for example memory, thread etc. 
 ```java
  public void destroy() 
  ```