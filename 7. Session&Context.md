#   Request
-   Make the form method as "get" and run with test value

![](Images\41.png)
-   Remove the parameter from the URL, and access again

![](Images/42.png)
This is happening because the **request** object is created again.
There are instances where we need to save this like during a login.

#   Session
-   Get the session object from request
    
    ```HttpSession	session	=	request.getSession();```  
-   Place the userName in the object to retrieve later
    
    ```		session.setAttribute("savedName", nameString);```
    
-   Pull the same user name and print when not null
    ```java
    if(nameString	!=	""	&&	nameString	!=	null)
		session.setAttribute("savedName", nameString);
		
	writer.write("Welcome "+nameString);
	writer.write("<br> Session parameter has user name as "+(String)session.getAttribute("savedName"));
    ```

-   Run this with giving a test name
![](Images\43.png)
-   Run it with an empty name, we can see the name is pulled from session
![](Images/44.png)

#   Context
-   We can see that session object saves name, but it doesn't in another browser. It is mapped only to that particular session.
-   Check the working of session in a new browser
![](Images/45.png)
-   When we want an object to be persistent across different users or even different browsers. We use Context
-   Add the code to get context in the servlet
![](Images/46.png)
-   Let us run this on eclipse browser by passing value.
![](Images/47.png)
-   We check in a new browser without passing any parameter, can see that the context has saved the name.
![](Images/48.png)
-----------